# 3. Intermediate Python

In this section we will expand your Python toolset with intermediate and advanced concepts that will help you write better
and cleaner code. It is the final chapter that concerns 'pure' Python before moving on to what you came for, the scientific 
packages.

- Functions and how to make your own
- Advanced looping: zip & enumerate
- Advanced looping: comprehensions
- Working with files
- Using IPython magic
- Project ideas