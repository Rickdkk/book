{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# Reading and writing files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Reading from and writing to files is an essential skill to have. Luckily, this is usually really easy because people try to save data in common formats such as .csv (comma separated values), .xlsx (Microsoft Excel), or json (JavaScript Object Notation). The Numpy and Pandas libraries that we will cover later have amazing functions that will help you read in those data with blazing speed. However, opening files and inspecting their contents without the help of those external libraries is sometimes needed when the file contents do not fit a standard format. This chapter will help you read and write data from those files.\n",
    "\n",
    "**You can download the file we will be needing for this section [here](https://gitlab.com/Rickdkk/book/-/raw/master/data/zen_of_python.txt) or you can use the code snippet below:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import requests  # https://requests.readthedocs.io/en/master/\n",
    "\n",
    "\n",
    "response = requests.get(\"https://gitlab.com/Rickdkk/book/-/raw/master/data/zen_of_python.txt\")\n",
    "\n",
    "with open(\"../data/zen_of_python.txt\", \"w\") as text_file:\n",
    "    text_file.write(response.text)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A good first step with text data is to open the file and look at it with a simple text editor (like Notepad). This way we can see whether we can use an existing function to do the heavy lifting for us or whether we need to write our own."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{figure} ../images/gedit_screenshot.png\n",
    "---\n",
    "name: gedit_screenshot\n",
    "---\n",
    "The data in a simple text editor (gedit).\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First we have to open the file with the [open](https://docs.python.org/3/library/functions.html#open) built-in function. The function accepts a path to a file and the method we want to apply, in this case `\"r\"` for read. I stored the data in the data folder which is parralel to this chapter folder so I have to go one directory up with `..` and then go into the data folder and specify the name. My folder structure:\n",
    "\n",
    "```\n",
    "- chapter_1\n",
    "- chapter_2\n",
    "- chapter_3 (current working directory)\n",
    "    - this_file\n",
    "- ....\n",
    "- data\n",
    "    - target_file\n",
    "```\n",
    "\n",
    "**Open the file with the `open` function and assign the handle to the variable `text_file`:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "text_file = open(\"../data/zen_of_python.txt\", \"r\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a couple of options when it comes to reading the file, we can use the `.read` method to read everything at once, the `.readlines` method to read all lines and return a list, or the `.readline` method to read line-by-line. \n",
    "\n",
    "**Repeatedly use the `.readline()` method untill you reach the end of the file:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "''"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "text_file.readline()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What you just saw is the Zen of Python, it highlights the Python philosophy to programming (you can access it with `import this`). If you reached the end, the method only returns an empty string: the file is exhausted and there is nothing left to read. Once we have all our data, we need to clean up after ourselves and make sure we close the file with the `.close` method.\n",
    "\n",
    "**Close the file:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [],
   "source": [
    "text_file.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Now reopen the file and use the `.read` method, close the file, and print the result:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Beautiful is better than ugly.\n",
      "Explicit is better than implicit.\n",
      "Simple is better than complex.\n",
      "Complex is better than complicated.\n",
      "Flat is better than nested.\n",
      "Sparse is better than dense.\n",
      "Readability counts.\n",
      "Special cases aren't special enough to break the rules.\n",
      "Although practicality beats purity.\n",
      "Errors should never pass silently.\n",
      "Unless explicitly silenced.\n",
      "In the face of ambiguity, refuse the temptation to guess.\n",
      "There should be one-- and preferably only one --obvious way to do it.\n",
      "Although that way may not be obvious at first unless you're Dutch.\n",
      "Now is better than never.\n",
      "Although never is often better than *right* now.\n",
      "If the implementation is hard to explain, it's a bad idea.\n",
      "If the implementation is easy to explain, it may be a good idea.\n",
      "Namespaces are one honking great idea -- let's do more of those!\n"
     ]
    }
   ],
   "source": [
    "text_file = open(\"../data/zen_of_python.txt\", \"r\")\n",
    "contents = text_file.read()\n",
    "text_file.close()\n",
    "\n",
    "print(contents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{margin} Parsing\n",
    "Parsing is the process of analyzing a piece of text and building a data structure. For example, reading a file and extracting all the numbers to a list for analysis.\n",
    "```\n",
    "\n",
    "Now you instantly have all the content in one place. We can parse the data ourselves to extract what we need. For example, let's say we want to split the lines to a list so we can get a random piece of advice from our program everytime we run it.\n",
    "\n",
    "**First split the contents for every newline (`\\n`), assign it back to contents and print the results:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['Beautiful is better than ugly.', 'Explicit is better than implicit.', 'Simple is better than complex.', 'Complex is better than complicated.', 'Flat is better than nested.', 'Sparse is better than dense.', 'Readability counts.', \"Special cases aren't special enough to break the rules.\", 'Although practicality beats purity.', 'Errors should never pass silently.', 'Unless explicitly silenced.', 'In the face of ambiguity, refuse the temptation to guess.', 'There should be one-- and preferably only one --obvious way to do it.', \"Although that way may not be obvious at first unless you're Dutch.\", 'Now is better than never.', 'Although never is often better than *right* now.', \"If the implementation is hard to explain, it's a bad idea.\", 'If the implementation is easy to explain, it may be a good idea.', \"Namespaces are one honking great idea -- let's do more of those!\"]\n"
     ]
    }
   ],
   "source": [
    "contents = contents.split(\"\\n\")\n",
    "print(contents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's print a random piece of advice. For a simple randomization task like this we can use the standard-library module `random`. There is a function called `choice` that picks a random object from a list.\n",
    "\n",
    "**Print a random piece of advice:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {
    "tags": [
     "hide-input"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Special cases aren't special enough to break the rules.\n"
     ]
    }
   ],
   "source": [
    "import random\n",
    "\n",
    "print(random.choice(contents))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using a context manager"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is really easy to forget to close the file. It is also pretty easy to damage the file if we start writing to it and if we hit an exception somewhere in our script. To make sure Python always closes the file we can wrap everything in a `try-except-finally` block or simply use a context manager. The latter is much shorter in this case and easier on the eye:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(\"../data/zen_of_python.txt\", \"r\") as text_file:\n",
    "    contents = text_file.read()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## File modes\n",
    "\n",
    "[here](https://docs.python.org/3/library/functions.html#open):\n",
    "- read only: `\"r\"`\n",
    "- write only: `\"w\"`\n",
    "- append: `\"a\"`\n",
    "- read + write: `\"r+\"`\n",
    "- binary mode: `\"b\"`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Writing to a file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Edit Metadata",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
