from glob import glob


def inplace_change(filename, old_string, new_string, verbose=False):
    # Safely read the input filename using 'with'
    with open(filename) as f:
        s = f.read()
        if old_string not in s and verbose:
            print('"{old_string}" not found in {filename}.'.format(**locals()))
            return

    # Safely write the changed content, if found in the file
    with open(filename, 'w') as f:
        if verbose:
            print('Changing "{old_string}" to "{new_string}" in {filename}'.format(**locals()))
        s = s.replace(old_string, new_string)
        f.write(s)


files = glob("./_build/html/**/*html", recursive=True)

for file in files:
    inplace_change(file, "github", "gitlab")
