# Bonus material

This section houses all the stuff that I thought was pretty nice to know, but isn't strictly needed for everyone. For 
example, I think version control is absolutely necessary for programming, but in reality almost zero scientists use this.
In that sense, the sections coming up are for the enthusiasts that want to know a little more about what you can do with
Python.