{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Symbolic math"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[SymPy](https://www.sympy.org/en/index.html) is a Python library for symbolic mathematics. It aims to become a full-featured computer algebra system (CAS) while keeping the code as simple as possible in order to be comprehensible and easily extensible. SymPy is written entirely in Python. It is slightly outside of the scope of what I would normally recommend for Human Movement Sciences (that's why it's an appendix), but I do feel that it deserves a honorable mention and based on the example below I think you can actually use it to solve HMS related problems."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Throwing a ball\n",
    "\n",
    "In this assignment we'll use a simple application of sympy. We will use it to determine how far we can throw a ball given an initial angle and velocity. We will also solve the maximum height that the ball reaches during our throw."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{figure} https://upload.wikimedia.org/wikipedia/commons/0/00/Ferde_hajitas3.svg\n",
    "---\n",
    "height: 250px\n",
    "name: projectile_motion\n",
    "---\n",
    "An example of projectile motion\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sympy import *\n",
    "from numpy import deg2rad\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Symbolic Python allows us to solve math problems in Python. We can write down an equation like we would in a notebook and instead of solving it by hand we can ask Python to solve it for us!\n",
    "\n",
    "The ball's horizontal and vertical displacement can be described as:\n",
    "\n",
    "$$\n",
    "  x(t) = v_0 t cos(\\phi)\n",
    "$$\n",
    "$$\n",
    "  y(t) = v_0 t sin(\\phi) - \\frac{1}{2}gt^2\n",
    "$$\n",
    "\n",
    "First we have to make some symbols to do math with:\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Making a variable"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can make variables using the `symbols` function. It accepts a string with the variable names you want and it let's you assign them to a variable in Python. We need a variable for v0, t, phi, and g:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle v_{0}$"
      ],
      "text/plain": [
       "v0"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "v0, t, φ, g = symbols(\"v0 t φ g\")\n",
    "v0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "sympy.core.symbol.Symbol"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "type(v0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Making an equation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can make our equation, let's start with x(t):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle t v_{0} \\cos{\\left(φ \\right)}$"
      ],
      "text/plain": [
       "t*v0*cos(φ)"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x = v0 * t * cos(φ)\n",
    "x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Cool! Sympy makes your equation look fancy! If it looks different for you and you want the best pretty printing for your environment use the init_printing() function. Note that the equality sign here is not mathematical equality, we simply assigned the equation to the variable x."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Your turn, do the same thing for y(t), the release height of the ball was 2m so add that too:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "tags": [
     "hide-input"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle - 0.5 g t^{2} + t v_{0} \\sin{\\left(φ \\right)} + 2$"
      ],
      "text/plain": [
       "-0.5*g*t**2 + t*v0*sin(φ) + 2"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "y = v0 * t * sin(φ) - (1/2) * g * t**2 + 2\n",
    "y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Solving an equation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The ball is thrown at 30 m/s at an optimal angle of 45 degrees. How long does it take before the ball hits the ground? To answer this question we must first fill in the formula, and then solve the remaining equation. You can substitute a symbol with a numerical value (or another symbol) with the `.subs` method. I'll do the first one:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle - 0.5 g t^{2} + 30 t \\sin{\\left(φ \\right)} + 2$"
      ],
      "text/plain": [
       "-0.5*g*t**2 + 30*t*sin(φ) + 2"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "y = y.subs(v0, 30)\n",
    "y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Now you have to subsitute φ (45 degrees) and g (9.81 m/s/s):**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "tags": [
     "hide-input"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle - 4.905 t^{2} + 21.2132034355964 t + 2$"
      ],
      "text/plain": [
       "-4.905*t**2 + 21.2132034355964*t + 2"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "y = y.subs(φ, deg2rad(45))\n",
    "y = y.subs(g, 9.81)\n",
    "y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now solve for when y(t) is equal to 0. Quite predictably, we do this with the function `solve`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[-0.0923105885630672, 4.41712270591198]"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "solve(Eq(y, 0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we know the ball hits the ground after about 4.42 seconds. This equation has two answers (because it is a parabola), but we're not really interested in what would happen if we threw the ball backwards along the same path (though this would probably happen if I tried to throw it). With this information we can calculate how far the ball traveled in the x-direction by substituting everything in x.\n",
    "\n",
    "**Substitute v0, φ, and t in the function x(t):**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "tags": [
     "hide-input"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle 93.7623591853362$"
      ],
      "text/plain": [
       "93.7623591853362"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x = x.subs(v0, 30)\n",
    "x = x.subs(φ, deg2rad(45))\n",
    "x = x.subs(t, 4.42)\n",
    "x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Almost 94 meters, that is pretty impressive! However, we did not account for air drag so it is probably a little optimistic. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Derivatives\n",
    "\n",
    "To calculate how high the ball traveled we must find the max of y. Remember that you can find the maximum of a function by looking at its derivative and determining when it is equal to zero. Sympy can also differentiate a formula for you with `diff`, diff accepts the formula and the symbol you want to differentiate for (in this case t).\n",
    "\n",
    "**Differentate y(t) and assign the result to the variable dy:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle 21.2132034355964 - 9.81 t$"
      ],
      "text/plain": [
       "21.2132034355964 - 9.81*t"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "dy = diff(y, t)\n",
    "dy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Now solve for 0:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "tags": [
     "hide-input"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[2.16240605867445]"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "solve(Eq(dy, 0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you know that the peak is at 2.16 seconds. All you have to do is substitute t with 2.16 in your original function.\n",
    "\n",
    "**Make the substitution in y(t):**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle 24.9357514208883$"
      ],
      "text/plain": [
       "24.9357514208883"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "y.subs(t, 2.16)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is your answer! 25m high is not too shabby 😎. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Quick plots"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To verify your answer, Sympy allows you to make some quick plots:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4yLjEsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy+j8jraAAAgAElEQVR4nO3dd3xUVf7/8ddJL6RACAmkkNB7DR1RXFAsdAugCDZs2Bu66trFtRcsrKBItQAK2EDEhrTQS+gJSSgphBJIz5zfHzfuz69LCWRmzpTP8/HgQUgmM++dlby5937uOUprjRBCCOFqfEwHEEIIIU5FCkoIIYRLkoISQgjhkqSghBBCuCQpKCGEEC5JCkoIIYRLkoISQgjhkqSghBBCuCS/6jxIKVUP6AU0AIqBLUCq1trmwGxCCCG8mDrTShJKqb7ABKAOsB7IBYKAZkBj4EvgNa31ccdHFUII4U3OVlCvAO9orTNP8TU/4ErAV2s99xRfTwA+BWIADUzWWr+llHoauBXIq3ro41rrb2v6P0QIIYRnOWNB/fdBSiVrrdPP9rm/fb0+UF9rvU4pFQasBYYA1wAntNav1iy6EEIIT1bdIYn/OULCOr13Wlrrg1rrdVUfFwJpQNy5xRNCCOGtzjgkoZRqAbQGIpRSw/7ypXCsa1HVopRKAjoCq7CGLcYrpW4AUoEHtdZHTvE944BxAK1ateq8devW6r6cEGdVWl7J7rwT7Mo5QfaRIgpLKsg/UUpeYSkVNo1SUFxWSViQH0opVNX3FZVVUlpeSZC/LyUVldQLCyLI35cGkUFEhwUSHRZEs5haJEeFEujva/R/oxAuTJ39IWe/BjUY67TcIGDBX75UCMzRWv9x1hdQqhbwC/CC1nqeUioGyMe6LvUc1mnAm870HCkpKTo1NfVsLyXEae0/UsTqjAIy8otYuj2HffknOVFaSXiwPzHhgcSEBREbEURIoC8Rwf5EBgcQHOBDkJ8v/r4KpRRaa8oqNEXlFZSU2yg4WUZpRSV7805SUmHDZtOszzxCuU2T0rA2CbVDCA/2p3PD2nRJrk1seLDpt0EIV1Hzgvrvg5TqobVecc4JlPIHFgE/aK1fP8XXk4BFWus2Z3oeKShxrsorbazYc5gf03L4dWceMeFBrEovoHPDSIL9/WgbH0HrBuE0rVeLJDsd7RSXVZCRf5KduSfYm3eCfQVF/LAlh+LySrok1eZYcTl9mkbTr1UMnRvWxt9XbkMUXssuR1BPAJNOdQqu6usXAyFa60Wn+JoCpgEFWuv7/vL5+lrrg1Uf3w9001qPOFNIKShRHWUVNpbvyefbTQdZvC2HY8XltKofTmxEEP1a1qNDQm2ax4bh61Otvxt2UV5pI+3gcVIzjvDT9lxWpxfQon4YmQVFXNIqhsva1qdXoygC5HSg8C52O8X3CFACrMMaDQ8CmgIdgB+BF7XWeaf43t7Ab8Bm4M8beh8HRlZ9rwYygNv+LKzTkYISZ7Irp5AZK/exKr2A7YcKCQv0o1+rGC5vW5/eTaIIDqjW/ehOUVRWwe+78vluyyF+3JaDRhMW5M/A9g24JiWBJvVqmY4ohDPYpaCma61HK6UewbpJtz7WShJpwK9a62J7JD0bKSjxd+WVNpZsy+HTFRms3FtAgK8P1/dIpGejulzQrC6Bfq5/RFJaUclvu/L5IjWLpWm5VNg016TE84+WMfRvGYOPE4/0hHAyuxTUNqAf8B3Q9+9f11oXnG+6cyEFJf50orSCrzfs5+2lu8g5XkpcZDDXd2/INSnxRNUKNB3vvOUVljJ/fTY/bD3E2n1HaVqvFrdf2JhBHRrItSrhiexSUPcAdwCNgP1/e3KttW5Uk4TVJQUlyittzFmdyVtLd2GzadrGR3JDj4Zc1LyeU68pOVpFpY1vNh/k/Z/3sP1QIXGRwdx2YSOuSUkgSK5TCc9h1ym+97XWd9Q40nmSgvJeWmu+33KIf/+wg/T8k3RNrsNjl7WgY2Jt09EcSmvNT9tzee/nPazdd4S6tQK49YJkru+eRGig61xTE+I82a+gTJOC8k5rMgp46ds01mVap7wmXNaCi1vUwxoQ9Q5aa1anFzDp5z2cLCkn60gxjw5owdCOcXKNSrgzKSjhnnbnFvLy9ztYsi2HmPBAHujfjOGd4vHz8msxa/cV8OyiNDZmHaVDQiRPD2pNh4RI07GEOB9SUMK95B4v4Y0fd/HZmkxCAvy446LG3NQrmeAAufbyJ5tNM3/9fiZ+v528wlKGd4rn0QHNqRde7ZXHhHAFUlDCPdhsmtlrMnnxmzTKKm1c160hd1/cxK2n8hztRGkF7/60m6m/p9OgdhD3XtyUoZ3iTccSorqkoITryy0s4cHPN/LbrnxGdk3k9gsb0TAq1HQst5GRf5LXFu9g4aaDjOnRkCeubCVj6cIdVKugZBxIGPPLzjwe/HwDJ0ormDisLdd2SfCqAQh7SKobyhvXdiA2Ioj//JZO2qFC3ruuE3Xl6FN4APmnlnC68kobL32Xxpipq6kTGsCC8b0Z0TVRyuk8+fn68M8rWvHmtR3YmHWUQe/8zubsY6ZjCVFjUlDCqTIPF3HVByv48Je9XNctkQXje9MsJsx0LI8wpGMcc+/oiVKKqz74g3nrsk1HEqJGpKCE0yzceIAr3v6NvXkneP+6TrwwtK2sjmBnbeIiWDC+Fx0TI3ng8408t2gbFZW2s3+jEC5IrkEJhysuq+SZhVuZsyaLTomRvDWiIwl1QkzH8lhRtQKZfnM3XvgmjSm/p5N28DjvjupEndAA09GEOCcyxSccavuh44yftZ49eSe448LG3N+/mUyZOdEXqVn886stRNcKZPINnWndIMJ0JCGgmlN88pNCOITWmukr9zH43eUcKy5n+k3deGRACyknJ7s6JYEvbutBpU0z/P0/WLDxgOlIQlSb/LQQdldcVsGdM9fx5Fdb6NYoiu/uvYDeTeuajuW12idEsvDu3rSNi+Ce2et56ds0Km2uf+ZECDnFJ+yqsKScG6auxs/Hh/6t6nFL70ayqKmLKKuw8eyircxYmckFTevyzsiORIbIdSlhhJziE85VVFbBTZ+sYXP2MW69IJlxfRpLObmQAD8fnh/SlonD2rJqbwGD3l3O9kPHTccS4rSkoIRdlJRXcsu0VNbuO8KbIzpwSetY05HEaYzomsjscd0pKa9k2Ht/8O3mg6YjCXFKUlCixkorKrlt+lpW7D3Ma9e058p2DUxHEmfRuWFtFt7dmxaxYdw5cx2v/LBdrksJlyMFJWqkrMLGXTPX8cvOPCYOa8vQjrKitruICQ9i9rjujOiSwKRle7hl2hqOFZebjiXEf0lBifNWUWnjvs/W82NaLs8Nbs21XRJNRxLnKNDPl5eGteX5IW34bVc+QyYtZ1dOoelYQgBSUOI8Vdo0D36xkW83H+KJK1oyukeS6UjiPCmluL57Q2aP605hSQVDJi3nh62HTMcSQgpKnDubTTNh7ia+3nCAhy9tzi0XNDIdSdhBl6Q6LLy7F03q1eK26Wt5fclO3OE2FOG5pKDEOdFa8+TXW/hibTb3/qMpd/VtYjqSsKP6EcF8dlsPhnWMY8WefP79ww7TkYQXk4IS1aa15tlF25i5KpPbL2zMff2amo4kHCDI35dXr25H03phvP/zHj5fk2U6kvBSspq5qBatNS9/v4OPl2dwY68kHh3QXDYY9GA+Pj48M7g1WUeKeHz+ZhLqhNCjcZTpWMLLuMcRVGmp6QRe780fd/HBL3u4rlsiT13ZSsrJC/j7+vDuqE40jArhjplrSc8/aTqS8DLuUVC7dsHRo6ZTeK1Jy3bz1tJdXN05nucGt5Fy8iIRwf5MHdsFBdz8yRqOFpWZjiS8iHsUVFkZXHMNVFSYTuJ1PvptL6/8sIPBHRowcXg7WVvPCzWMCuXD0SlkHSnijhnrKJcdeoWTuEdBJSbCkiVw//2mk3iV6SsyeP6bNC5vG8trV7fHV8rJa3VNrsNLw9qxYu9hnvxqi4yfC6dwj4KqWxcefBDefRfee890Gq/w2ZpMnvx6K/1axvDWiI74yUaDXu+qzvHceVFj5qzJYsrv6abjCC/gPlN8L78M27fDPfdAs2bQr5/pRB5r/vpsJszbzIXNopl0XUfZBVf810OXNCc9/yQvfJtGUlQo/VrFmI4kPJj7/OTx9YVZs6BlS7j6atghNxA6wjebDvLg5xvpnhzFh6M7E+jnazqScCE+PorXr+lAmwYR3DNnPdsOyH5SwnHcp6AAwsNh4ULw94eBA6GgwHQij7J46yHunbOezg1rM2VsCkH+Uk7ifwUH+PLRmBTCg/y5edoaco+XmI4kPJR7FRRAUhLMmwf79llHUuWyPYA9LNuRy12z1tEmLoKpY7sQEuA+Z3+F88WEB/HRmBSOFpVz66epFJdVmo4kPJDDCkoplaCUWqaU2qaU2qqUurfq83WUUkuUUruqfq99zk/euzdMngw//QR33w0yUVQjy3fnc9v0tTSPDWPaTV0JC/I3HUm4gTZxEbw1ogOb9h/jwS82YJMND4WdOfIIqgJ4UGvdCugO3KWUagVMAJZqrZsCS6v+fO7GjIFHH4UPP7Sm+8R5WbX3MDdPW0OjuqFMv6kbEcFSTqL6Lmkdy2OXteDbzYd448edpuMID+OwgtJaH9Rar6v6uBBIA+KAwcC0qodNA4ac94u8+CIMHgz33Qfff1/DxN5n7b4j3PTJGuIig5lxSzdqhwaYjiTc0K0XNOLalATe+Wk389dnm44jPIhTrkEppZKAjsAqIEZrfbDqS4eA859T9fGBGTOgbVu49lpIS6tpVK+xKfsoY6euJjoskFm3dqdurUDTkYSbUkrx3JA2dG9Uh0e/3MyaDBleEvbh8IJSStUC5gL3aa3/z0yqtm5HP+WJa6XUOKVUqlIqNS8v7/QvUKsWLFgAQUFw5ZVw+LAd03umbQeOM3rKaiJC/Jl1a3diwoNMRxJuLsDPhw+u70xc7WBum76WzMNFpiMJD+DQglJK+WOV00yt9byqT+copepXfb0+kHuq79VaT9Zap2itU6Kjo8/8QomJ8NVXkJ0Nw4dba/eJUzp8opRXfthOZIg/s2/tToPIYNORhIeIDAlgypgUKm2am6at4XiJTNiKmnHkFJ8CpgBpWuvX//KlBcCYqo/HAF/b5QV79IApU+CXX2D8eJnsOwWtNY/O3cTy3Yd5d2QnEuqEmI4kPEyj6Fq8f30nMvJPctfMdVTIwrKiBhx5BNULGA1crJTaUPXrcmAi0F8ptQvoV/Vn+7j+enj8cfjPf+Ctt+z2tJ5ixqpMfkzL5dHLWtA2PsJ0HOGhejauy/ND2vDbrnyeXbTNdBzhxhx2N6bW+nfgdMtf/8NRr8tzz1nDEg8+aK3Zd/nlDnspd7Izp5DnF23jwmbR3NgzyXQc4eFGdE1kb/5JJv+6l0Z1QxnbK9l0JOGG3G8libPx8YHp06FdOxgxArZuNZ3IuJLySu6ZvZ6wID9evbq97OkknOLRAS3o1zKGZxdtY9mOU15qFuKMPK+gAEJDrcm+0FBrzb4zTQF6gZe/3872Q4W8clV7osNknFw4h6+P4q0RHWgRG87ds9az41Ch6UjCzXhmQQEkJMDXX8PBgzBsGJSWmk5kxLLtuXy8PIOxPZPo26Ke6TjCy4QG+jFlbAohAb7c9Mka8k94599DcX48t6AAunaFjz+2Vj1/8EGvm+zLKyzl4S830iI2jAmXtTAdR3ip+hHBfDQmhcMnSxn3aSol5bKwrKgezy4osK5DXXUVTJoEr71mOo3T2Gyah77YSGFJBW+N6ChbZwij2sVH8vo1HViXeZRHvtwkW8aLavH8ggL417+srTkeecTaT8oLfPJHBr/szOOJK1rSPDbMdBwhuLxtfR6+tDkLNh7g7aW7TccRbsA7CsrHBz75BDp1glGjYNMm04kcKu3gcSZ+t51+LetxffeGpuMI8V93XtSYYR3jeOPHnSzYeMB0HOHivKOgAEJCrKGJ8HBrsi8nx3Qihygus0bKI0L8eXl4O6wFPYRwDUopXhreli5JtXnoi42syzxiOpJwYd5TUABxcVZJ5eVZk30lnrdV9QvfbmNX7glev6Y9UbJCuXBBgX6+fDg6hdjwIMZ9mkr2EVlYVpyadxUUQEoKTJsGf/wB48Z51GTfkm05zFiZya0XJHNB07MssCuEQXVCA5g6NoWS8kpeW7yDsgpZs0/8L+8rKLAGJp55xlpx4uWXTaexi5zjJTzy5UZaNwjnoUubm44jxFk1qRfGa9d0YP76A7z/8x7TcYQL8s6CAnjySWsE/bHHYP5802lqxGbTPPD5BkrKbbw9siOBfjJSLtzDpa1jubJdfSYt283uXFlpQvxf3ltQSsHUqdCli7UK+vr1phOdt49+38vy3Yd5amArGkfXMh1HiHPyr4GtCQ7wZcLczdhsnnPKXdSc9xYUQHCwNTRRuzYMGgSHDplOdM627D/GKz/sYEDrWEZ0STAdR4hzFh0WyBNXtCR13xFmrc40HUe4EO8uKID69a2bdwsKYMgQt5rsKyqr4J7Z64kKDWTi8LYyUi7c1lWd4+nVJIqJ323n0DH3+TsoHEsKCqBjR2tgYtUquPlmt5nse3bhNtIPn+T1a9sTGRJgOo4Q500pxYtD21JeaePJr7fIUkgCkIL6/4YNgxdegFmz4MUXTac5q+82H2TOmizuuLAxPRvXNR1HiBprGBXK/f2bsWRbDt9vcb/T7cL+pKD+6rHH4Lrr4IknYO5c02lO68DRYibM20z7+Aju79/MdBwh7OaW3sm0bhDOUwu2cqy43HQcYZgU1F8pBR99BN27w+jRsG6d6UT/o9Kmuf+zDZRX2nhrREf8feX/QuE5/Hx9eHl4OwpOljHxuzTTcYRh8tPt74KC4KuvoG5da7LvgGstaPnBL3tYlV7As4PbkFQ31HQcIeyuTVwEN/dOZvbqLFbuPWw6jjBICupUYmKsyb6jR2HwYChyjbXC1mce4fUlO7myXX2Gd4ozHUcIh7m/XzMS64Tw2LzNssGhF5OCOp327a2BibVr4cYbjU/2nSit4N45G4gND+KFoTJSLjxbcIAvLwxtQ3r+Sd75aZfpOMIQKagzGTQIJk6Ezz+HZ581GuWpr7eQfaSIN0d0ICLY32gWIZzhgqbRDO8Uz4e/7CXt4HHTcYQBUlBn8/DDMGYMPP00fPaZkQhfb9jPvHX7GX9xU7ok1TGSQQgTnriiJRHB/kyYu4lKWQbJ60hBnY1S8OGH0KsXjB0La9Y49eWzCop4Yv4WOiVGcs/FTZz62kKYVjs0gKcGtmJj9jGm/ZFhOo5wMimo6ggMtFY8j421hiays53yslpr3v95D6GBfrw1oiN+MlIuvNCg9g3o2zyaVxfvkM0NvYz8xKuu6Ghrsq+w0Cqpkycd/pLfbznErNWZ3H5RIxLqhDj89YRwRUopnh/aFoAnvpJlkLyJFNS5aNMG5syxtuYYMwZsjtsFtKS8khe+TaN5TBjXd2vosNcRwh3ERQbz8KXN+XlHHgs2uta9icJxpKDO1RVXwKuvWkshPf20w15myu/pZB8p5qmBreTUnhDADT2S6JAQyTMLt1Fwssx0HOEE8pPvfNx/v7Xq+XPPWfdK2VnO8RImLdvNJa1i6NVEFoIVAsDXRzFxeFuOF5fz/DfbTMcRTiAFdT6Ugvfegz594O67YfVquz79v7/fQUWl5p9XtLTr8wrh7lrEhnPHRY2Zt24/v+7MMx1HOJgU1PkKCLBO83XoYN3Qm5Vll6fdmHWUueuyubF3Eg2jZK09If7urr5NaBQdyj+/2kxRWYXpOMKBpKBqom5deOcdKC6GgQPhxIkaPZ3WmmcXbaNurUDG95V7noQ4lSB/X14a2pasgmLeWLLTdBzhQFJQNdWqlbXCxObN1hYdNZjsW7DxAGv3HeHhS5sRFiTLGQlxOt0aRTGyayJTfk9nc/Yx03GEg0hB2cOAAfD669Y2HU88cV5PUVxWycTvttMmLpyrOifYOaAQnmfCZS2oWyuQR+duorzScbd8CHOkoOzlnntg3Dh46SWYPv2cv/3DX/dw8FgJT13ZGl8fWalciLOJCPbn2cGt2XbwOFN+TzcdRziAFJS9KAXvvgsXXQS33AJ//FHtbz1wtJgPftnDFe3q0zVZFoMVoroGtKnPpa1jeGPJTjLyHb+6i3AuhxWUUmqqUipXKbXlL597Wim1Xym1oerX5Y56fSP8/eHLLyExEYYMgX37qvVtE7/bjtbw2GUtHBxQCM/z7OA2BPj68Pj8zbIMkodx5BHUJ8CAU3z+Da11h6pf3zrw9c2IirLW7Csrsyb7CgvP+PC1+wpYsPEA4/o0Ir62rLcnxLmKCQ9iwuUt+GPPYb5Y65yFnIVzOKygtNa/AgWOen6X1qIFfPEFbNsGo0ZB5am3rLbZNM8s3EZMeCC3X9jYySGF8BwjuyTSNakOL3yTRl5hqek4wk5MXIMar5TaVHUKsPbpHqSUGqeUSlVKpeblueEd4/37w1tvwaJF8Nhjp3zIvPX72ZR9jAmXtSA00M/JAYXwHD4+iheHtaW4rJJnFm41HUfYibML6n2gMdABOAi8droHaq0na61TtNYp0dHRzspnX3fdBXfeCa+8Ah9//H++dKK0gn9/v50OCZEMbh9nKKAQnqNJvVrcfXETFm06yNK0HNNxhB04taC01jla60qttQ34D9DVma9vxJtvQr9+cNtt8Ntv//30e8t2k1tYyr8GtsJHxsqFsIvbLmxM85gwnvhqCydKZRkkd+fUglJK1f/LH4cCW073WI/h7w+ffw7JyTBsGKSnk1VQxEe/pzO0YxwdE097llMIcY4C/HyYOLwth46X8PbSXabjiBpy5Jj5bGAF0Fwpla2Uuhn4t1Jqs1JqE9AXuN9Rr+9Sate2rkVVVsLAgbw+dw2+SvHoABkrF8LeOibW5s4LG/NFahZ782q2PqYwS7nDfQMpKSk6NTXVdIya++knbJdeys+JHdj6/nTuvkQKSghHyCsspc+/l9G/VQxvj+xoOo74X9W6riErSThR5UV9eXfovVy8N5U7vvvQdBwhPFZ0WCA39kpi4aYDbD903HQccZ6koJzo89QsXm/Ul/RRN+P35pvw0UemIwnhscb1aUStAD9eWyxbcrgrKSgnOV5Szqs/7KBrUh2SPnkfLr0U7rgDfv7ZdDQhPFJkSAC39mnEkm05bMw6ajqOOA9SUE7yztJdFBSV8dTAVih/f2sPqaZNYfhw2LPHdDwhPNJNvZOpHeLPq4t3mI4izoMUlBOk55/kkz8yuLpzPG3iIqxPRkRYa/YBXHklHJNN14Swt1qBftxxUWN+25XPqr2HTccR50gKygle+GYbgX6+PHRp8//7hcaNYe5c2L0brr0WKuTGQiHs7YYeSdQLC+S1xTtltXM3IwXlYL/tyuPHtFzGX9yEemFB//uAiy6C99+HH36ABx90ej4hPF2Qvy93X9yE1RkF/Lor33QccQ6koByootLGswu30TAqhBt7JZ3+gbfcAvffD2+/DR984LR8QniLa7skEhcZzGuLd8hRlBuRgnKgmasy2ZV7gscvb0mgn++ZH/zKK3D55TBhgkz2CWFnAX4+3NuvKZuyj7F4mywk6y6koBzkaFEZb/y4k56No7ikVczZv8HXF2bPhu7drTX7dsq9G0LY07COcTSqG8rri3dSaZOjKHcgBeUgb/64i+PF5dZYuarmauXh4fDee1ZZDRwIR444NqQQXsTP14f7+jdjR04hizYdMB1HVIMUlAPsyilk+sp9jOyaSIvY8HP75kaNYN48SE+Hq6+G8nLHhBTCC13Ztj4tYsN4Y8lOKiptpuOIs5CCsjOtNc99k0ZogC8P9G92fk9ywQUweTIsXQr33WffgEJ4MR8fxQP9m5FxuIi567JNxxFnIQVlZ8t25PLrzjzu7deMqFqB5/9EY8fCww9bp/wmTbJbPiG8Xf9WMbRPiOTtpbsprag0HUecgRSUHZVV2HhuURqNokO5oUfDmj/hSy9Z16LuvReWLKn58wkhUErx0CXN2H+0mDmrs0zHEWcgBWVHn67IID3/JE9e0Qp/Xzu8tb6+MHMmtGplXY/avr3mzymEoHeTunRNrsO7y3ZTXCZHUa5KCspODp8o5a2lu7iwWTR9W9Sz3xOHhVlr9gUGWkdTBQX2e24hvJRSiocvbU5eYSmfrsgwHUechhSUnby2ZCdFZZU8eWVL+z95w4Ywfz5kZsJVV8lknxB20CWpDhc2i+b9X/ZQWCJ/p1yRFJQdZOSfZFP2UW7r04gm9cIc8yI9e1obHC5bBuPHgyzXIkSNPXhJM44WlTPl93TTUcQpSEHZwQe/7GFnzgnG9kxy7AuNHm0thTR5MrzzjmNfSwgv0C4+kktbxzDlt3SOnCwzHUf8jRRUDR04Wszcddlcm5JAvfBTrFZuby+8AEOGWIvLfv+9419PCA/3QP/mnCir4MNf95qOIv5GCqqG/vPbXmwaxvVp5JwX9PGB6dOhbVtrD6lt25zzukJ4qOaxYQxq34BP/kgnt7DEdBzxF1JQNZB/opTZqzMZ0iGOhDohznvhWrVgwQIIDrYm+/JljxshauK+fs0or9S8t2yP6SjiL6SgauDj5emUVti446LGzn/xxET46ivYvx+GD4cyOX8uxPlKrhvKVZ3imbUqk/1Hi03HEVWkoM7TseJyPv1jH5e1iaVJvVpmQnTvDlOnwq+/wh13yGSfEDVwT7+mALz70y7DScSfpKDO04yV+ygsreDOi5qYDTJqFDzxhFVUb7xhNosQbiwuMphR3RL5PDWbjPyTpuMIpKDOS1FZBVN+T+ei5tG0iYswHQeeecY6zffQQ/DNN6bTCOG27uzbGH9fxZs/yoahrkAK6jzMWZ1Fwckyxvc1fPT0Jx8fmDYNOnaEkSNhyxbTiYRwS/XCghjTM4mvNx5gZ06h6TheTwrqHJVWVDL51710Ta5DSlId03H+v9BQ+Ppra8Jv4EDIyzOdSAi3dHufxoQG+PH6YjmKMk0K6hzNW7efQ8dLXOfo6a/i462SOnQIhg2D0lLTiYRwO7VDA7i5dzLfbz3E5iV0cxwAABe0SURBVOxjpuN4NSmoc1BRaeODX/bQNi6CC5rWNR3n1Lp0sU73/f47jBsHNtnWWohzdcsFyUSG+PPakh2mo3g1Kahz8M3mg+w7XMRdfZuglDId5/SuucYanNi7F157zXQaIdxOWJA/t1/YmJ935JGaIVvcmCIFVU02m3WXedN6tbikVYzpOGf35JPQoAE8+qi16oQQ4pzc0KMhdWsF8soPO9Byj6ERUlDV9GNaDjtyCrmzb2N8fFz46OlPSsHHH0Pnzta9Ups2mU4khFsJCfBjfN/GrEovYPnuw6bjeCUpqGrQWjPp5z0k1AlmYLsGpuNUX0iINTQREWFN9uXkmE4khFsZ2S2RBhFBvLJYjqJMkIKqhuW7D7Mx6yi3X9gYP183e8saNLBO8eXlwdChUCKrNQtRXYF+vtzzj6ZszDrKj2m5puN4HYf9tFVKTVVK5Sqltvzlc3WUUkuUUruqfq/tqNe3p3eX7aJeWCBXdY43HeX8dO4Mn34KK1bArbfKmn1CnIPhneNJigrhtcU7sNnk744zOfJw4BNgwN8+NwFYqrVuCiyt+rNLW7uvgJV7CxjXpxGBfr6m45y/q66C556DGTNg4kTTaYRwG/6+PtzXrxnbDxXyzeaDpuN4FYcVlNb6V+Dv85mDgWlVH08Dhjjq9e1l0rI91A7xZ1S3RNNRau6f/7SWQnr8cZg3z3QaIdzGwPYNaBZTizd+3ElFpdxb6CzOvqASo7X+858ghwCXntfeeuAYP23P5aZeyYQE+JmOU3NKwZQp0K0bjB4N69ebTiSEW/D1UTzQvzl7804yf/1+03G8hrEr/toaiTntCV2l1DilVKpSKjXP0Lpy7/28h1qBftzQI8nI6ztEcLC10WFUFAwaBAfllIUQ1XFp6xjaxkXw1tJdlFXIUZQzOLugcpRS9QGqfj/tWIzWerLWOkVrnRIdHe20gH/am3eCbzcfZHSPhkSE+Dv99R0qNtaa7CsogCFDoFh2EBXibJRSPHhJM7KPFPNZapbpOF7B2QW1ABhT9fEY4Gsnv361vf/zHgJ8fbipV7LpKI7RoQPMnAmrV8NNN8lknxDVcGGzaLok1ebdn3ZRUl5pOo7Hc+SY+WxgBdBcKZWtlLoZmAj0V0rtAvpV/dnl7D9azPz1+xnZNZHosEDTcRxnyBB46SWYMweef950GiFcnnUU1Rw0LNhwwHQcj+ewK/9a65Gn+dI/HPWa9jL5lz0AjOvTyHASJ3j0Udi2DZ56Clq0gKuvNp1ICJfWvVEUCVEhvP3TLoZ3jsfXHZY+c1NutiyC4+UVljJnTRbDOsXRIDLYdBzHUwr+8x/o2RPGjIHUVNOJhHB5N/ZMJvtIMUvTZPkwR5KC+pspv6dTXmnjjotccENCRwkMhPnzIToaBg+G/TJGK8SZXNo6hgYRQXy8PMN0FI8mBfUXx4rKmbFyH5e3rU9y3VDTcZyrXj1YuBCOH7dKqqjIdCIhXJafrw+jeySxYu9h0g4eNx3HY0lB/cW0FRmcKK3gLlfczt0Z2rWDWbNg3TrrdJ/sxivEaY3smkCQvw/T/sgwHcVjSUFVOVlawdTl6fyjRT1a1g83HcecgQPh5Zfhyy+tXXmFEKcUGRLA0I5xzF+/n4KTZabjeCQpqCqzV2dytKicuy720qOnv3roIRg7Fp59FmbPNp1GCJc1tmcypRU2Zq/ONB3FI0lBASXllUz+dS89G0fRKdEtdgBxLKXggw+gd2+48UZYtcp0IiFcUvPYMHo1iWLGyn2UyyKydicFBXy5NpvcwlLvvfZ0KoGB1orn9etbN/RmydIuQpzK2J7JHDxWwg9bD5mO4nG8vqAqKm188MseOiRE0rNxlOk4riU62prsO3nSWlj25EnTiYRwORe3qEdinRAZOXcAry+oBRsPkH2kmLv6NkEpuSP8f7RpYy2FlJMDDzwgk31C/I2vj2JMzyTW7jvCpuyjpuN4FK8uKJtN897Pe2gRG8Y/WtQzHcd1XX65tSTS5MnWkkhCiP/j6pR4QgN8+USOouzKqwtq8bZD7M49wZ19m+Aj62md2T33wC23wAsvWKugCyH+KzzIn6s6x7Nw0wFyC0tMx/EYXltQWmsmLdtDUlQIV7StbzqO61MKJk2CCy+Em2+GFStMJxLCpYzpmUR5pWbWKhk5txevLahfd+Wzef8x7riosaxGXF0BATB3LsTHW5N9+/aZTiSEy2gUXYu+zaOZsTKT0grZK8oevLagJi3bTf2IIIZ2jDcdxb1ERVmTfSUl1mTfiROmEwnhMsb2Sib/RCnfbDpoOopH8MqCWpNRwOr0Asb1aUSAn1e+BTXTsiV8/jls2QLXXSeTfUJU6dO0Lo2jQ/l4eQZadqmuMa/86fzuT7uJCg1gRJdE01Hc16WXwptvwoIF8PjjptMI4RKUUoztlczm/cdYl3nEdBy353UFlXboONsOHuem3skEB/iajuPexo+H22+3FpedNs10GiFcwrCOcYQF+TFVRs5rzOsKatryDIpLKxjVVY6eakwpePttuPhiGDcOli83nUgI40ID/RjRJYHvtxzi4LFi03HcmlcV1PGScr7ecIAr2jWgdmiA6Tiewd8fvvgCGjaEoUMhI8N0IiGMu6FHElprpq+QSdea8KqCmr9uP8XllVzfvaHpKJ6lTh1rsq+83NpPqrDQdCIhjEqoE0K/ljHMXp1JSbmMnJ8vrykorTUzV+2jXXwEbeMjTMfxPM2bW5N9aWkwahRUyl9K4d1u7JXMkaJyvt6w33QUt+U1BZW67wg7c05wXTe59uQw/ftb16QWLYIJE0ynEcKo7o3q0CI2TEbOa8BrCmrGyn2EBfkxsH0D01E82513wl13wauvwtSpptMIYYxSipt6JbP9UCEr9h42HccteUVBHT5RynebDzG8UzwhAX6m43i+N9+Efv2sEfRffzWdRghjBnVoQO0Qf1nl/Dx5RUF9uTabskobo+T0nnP4+VnXoxo1gmHDYO9e04mEMCLI35dR3RJZkpZDVkGR6Thux+MLymbTzFqdSdekOjSLCTMdx3vUrm1N9tls1mTf8eOmEwlhxOjuSfgoxbQ/MkxHcTseX1DL9+Sz73AR13WXoyena9rUWv18504YMUIm+4RXio0I4rI2sXyWmsXJ0grTcdyKxxfUjJX7qBMawIA2saajeKe+feHdd+G77+Dhh02nEcKIG3slU1hSwbx12aajuBWPLqhDx0r4MS2Xq1PiCfSTdfeMue02uPdeeOMN+M9/TKcRwuk6JUbSPj6Cj//IwGaTkfPq8uiC+mxNFpU2LevuuYJXX4UBA6wx9J9/Np1GCKeyVjlPYm/eSX7bnW86jtvw2IKqqLQxZ00mFzStS8OoUNNxhJ8fzJljXZcaPhx27zadSAinuqJtA6LDAvl4ebrpKG7DYwvqp+25HDxWwnXdZN09lxERYU32KWVN9h09ajqREE4T4OfD9d0a8vOOPPbkyU7U1eGxBTVzVSYx4YH0a1nPdBTxV40bw7x5sGcPXHstVMhUk/Aeo7olEuDrw6cycl4tHllQmYeL+HVXHiO6JOLn65H/E91bnz7w/vvW+PmTT5pOI4TTRIcFcmX7+ny5NpvjJeWm47g8j/zpPXtNJgoY0TXBdBRxOjffDNdcAxMnwgcfmE4jhNPc1CuZk2WVfL4my3QUl+dxBVVWYePzNVn8o2UM9SOCTccRZ/Lii3DFFdbW8T/9ZDqNEE7RJi6CLkm1mbYig0oZOT8jIwWllMpQSm1WSm1QSqXa87m/33qIwyfLZFNCd+DrC7NmQcuWcNVV1ik/IbzA2J7JZBUU89P2XNNRXJrJI6i+WusOWusUez7pzJX7SKgTzAVN6trzaYWjhIdbk32+vtZk35EjphMJ4XCXto6hQUSQjJyfhUed4tudW8iq9AJGdW2Ij48yHUdUV1ISzJ8P6elw9dXW1vFCeDA/Xx9G90jijz2H2X5IFlI+HVMFpYHFSqm1Sqlxp3qAUmqcUipVKZWal5dXrSeduSoTf1/F1Snx9swqnKF3b5g8GZYuhfvuM51GCIcb0SWBIH8fWeX8DEwVVG+tdSfgMuAupVSfvz9Aaz1Za52itU6Jjo4+6xMWl1Uyd202l7WpT91agQ6ILBxu7Fh45BF47z2YNMl0GiEcqnZoAEM7xjFv3X6OnCwzHcclGSkorfX+qt9zgflA15o+58JNBzheUsF1simhe3vxRRg0yFpcdvFi02mEcKixPZMprbAxe02m6SguyekFpZQKVUqF/fkxcAmwpabPO3NVJk3q1aJrcp2aPpUwydcXZsyA1q2t+6S2bzedSAiHaR4bRq8mUUxfsY+KSpvpOC7HxBFUDPC7UmojsBr4Rmv9fU2ecMv+Y2zMOsp13RJRSoYj3F5YGCxYAIGBcOWVcPiw6URCOMzYnskcPFbCD1tzTEdxOU4vKK31Xq11+6pfrbXWL9T0OWeuyiTI34dhnWQ4wmM0bAhffQVZWdY9UmVyjl54potb1COxToiMnJ+C24+ZF5aU8/WG/Qxq34CIYH/TcYQ99egBU6ZY+0eNHw9a7roXnsfXRzGmZxKp+46wOfuY6Tguxe0L6qv1+ykqq5RtNTzV9dfD449bO/G+/bbpNEI4xNUp8YQG+PLxH3IU9VduXVBaa2auyqRNXDjt4iNMxxGO8txzMHQoPPAAfPed6TRC2F14kD9XdY5n0caD5BWWmo7jMty6oNZlHmH7oUKu69ZQhiM8mY8PTJ8O7dpZe0ht3Wo6kRB2N6ZnEmWVNmatkpHzP7l1Qc1YmUlYoB+D2jcwHUU4WmioNdkXGmqt2ZefbzqREHbVKLoWFzWPZsaqfZRVyMg5uHFBFZws45vNBxnaKY7QQD/TcYQzJCRYk30HDsCwYTLZJzzOjb2SySss5ZvNB0xHcQluW1Bz12ZTVmFjlKwc4V26dYOPP4bffoPbb5fJPuFR+jStS+PoUD5enoGW/7bds6BsNs2s1ZmkNKxNi9hw03GEs40caW0V//HH8PrrptMIYTdKKcb2SmZT9jHWZR41Hcc4tyyoFXsPk55/UjYl9GZPP23dwPvww7Bokek0QtjNsI5xNI8JY/HWQ6ajGOeWBTVj5T5qh/gzoE2s6SjCFB8fmDYNOnWyjqg2bzadSAi7CA30o2tyHT75I4PCEu/eG83tCirneAmLt+VwdUoCQf6+puMIk0JC4OuvrbX7Bg6EXNk+W3iG4Z3jKa2w8e3mg6ajGOV2BfX5miwqbZqRXWU4QgBxcdb4eU6ONdlXKjc5CvfXPj6CRtGhzF2333QUo9yqoCptmtmrM+ndpC7JdUNNxxGuIiXFOt23fDmMGyeTfcLtKaUY3ime1ekFZBUUmY5jjFsV1LLtuRw4VsL13eXoSfzNNdfAM8/Ap5/CK6+YTiNEjQ3pGIdSMM+Lj6LcqqBmrtpHvbBA/tEyxnQU4YqefBJGjIAJE6xrU0K4sbjIYHo0imLe+myvvSfKbQoqq6CIn3fmMaJLAv6+bhNbOJNSMHWqdcrvuutg40bTiYSokWGd4tl3uIh1mUdMRzHCbX7Sz1mTiQJGyHCEOJPgYOvoKTISRo2CQ3IviXBfl7WJJdjfly/XeudpPrcoKK3hszXZXNwihgaRwabjCFdXv7412ae1tU1HSYnpREKcl9BAPy5rE8uiTQcoKa80Hcfp3KKgjpeUk3+ilOtkOEJUV6dO8PzzsHIl3HKLTPYJtzWsUzyFJRX8mJZjOorTuUVBHT5RRnztYPo0jTYdRbiTYcOskpo5E156yXQaIc5Lj8ZR1I8I8sppPrcoqJNlFYzsmoivj2xKKM7R449b16L++U+YN890GiHOma+PYkjHOH7Zmed1u+26RUEp4JqUBNMxhDtSCqZMsbbpGD0a1q0znUiIcza8UxyVNs3XG7zrKMotCio82J/osEDTMYS7CgqyNjqMioJBg+Cgd69vJtxPk3phtI+P8LrTfG5RUFGhAaYjCHcXGwsLF8LRozB4MBQXm04kxDkZ1imebQePk3bwuOkoTuMWBSVbugu7aN8eZsyA1FS48UaZ7BNuZWD7Bvj7KuatyzYdxWncoqCEsJshQ6yJvs8+g+eeM51GiGqrExpA3+b1+GrDASoqbabjOIUUlPA+jzwCN9wA//oXfP656TRCVNuwTvHkFZby2+5801GcQgpKeB+lYPJk6NULxoyBNWtMJxKiWi5uUY/IEH+vGZaQghLeKTDQui8qJsYamtjvHX/hhXsL8PNhUPsGLN56iONesB28FJTwXvXqWZN9hYXW+HmR924MJ9zHsE5V28Fv8vzbJaSghHdr2xZmz4b1663TfTbvuPgs3Ff7+AgaR4d6xWk+KSghrrzS2oX3yy/h6adNpxHijJRSDOsUz+qMAjIPe/ZRvxSUEAAPPAA33WSNns+aZTqNEGc09M/t4Nd79j1RUlBCgDXZ9/770KePVVSrVplOJMRpNYgMpmfjKOat2+/R28FLQQnxp4AAmDsXGjSwJvuyskwnEuK0hnWMJ7OgiNR9nrsdvBSUEH9Vty4sWmSt1TdwIJw4YTqREKc0oE0sIQG+Hr30kRSUEH/XqhXMmQObN1tbdMhkn3BBoYF+DGgTy6JNBz12O3gjBaWUGqCU2qGU2q2UmmAigxBndNll8Prr1jYdTz5pOo0QpzS8ajv4Jds8czt4pxeUUsoXmARcBrQCRiqlWjk7hxBndc89MG4cvPiitQq6EC6mR6MoGkQEeexpPhNHUF2B3VrrvVrrMmAOMNhADiHOTCl491246CK4+WaZ7BMux6dqO/hfd+WTW1hiOo7dmdhoKQ7463hUNtDt7w9SSo0DxlX9sVQptcUJ2TxBXcA7ljqumXN/n7p3d0wS1yf/TVWfsfcq5iUTr3retmit25ztQS67E6DWejIwGUAplaq1TjEcyS3Ie1U98j5Vn7xX1SfvVfUopVKr8zgTp/j2Awl/+XN81eeEEEKI/zJRUGuApkqpZKVUADACWGAghxBCCBfm9FN8WusKpdR44AfAF5iqtd56lm+b7PhkHkPeq+qR96n65L2qPnmvqqda75Py5HWchBBCuC9ZSUIIIYRLkoISQgjhkly6oGRJpOpTSk1VSuXK/WJnppRKUEotU0ptU0ptVUrdazqTq1JKBSmlViulNla9V8+YzuTKlFK+Sqn1SqlFprO4MqVUhlJqs1Jqw9nGzV32GlTVkkg7gf5YN/OuAUZqrbcZDeailFJ9gBPAp9W5Ac5bKaXqA/W11uuUUmHAWmCI/Hf1v5RSCgjVWp9QSvkDvwP3aq1XGo7mkpRSDwApQLjW+krTeVyVUioDSNFan/WGZlc+gpIlkc6B1vpXoMB0DlentT6otV5X9XEhkIa1uon4G235c78R/6pfrvkvWsOUUvHAFcBHprN4ElcuqFMtiSQ/SITdKKWSgI6ALLJ3GlWnrTYAucASrbW8V6f2JvAIIHuznJ0GFiul1lYtaXdarlxQQjiMUqoWMBe4T2t93HQeV6W1rtRad8Ba8aWrUkpOH/+NUupKIFdrvdZ0FjfRW2vdCWtHi7uqLk+ckisXlCyJJByi6nrKXGCm1nqe6TzuQGt9FFgGDDCdxQX1AgZVXVuZA1yslJL9WU5Da72/6vdcYD7W5ZxTcuWCkiWRhN1VXfifAqRprV83nceVKaWilVKRVR8HYw0sbTebyvVorR/TWsdrrZOwfk79pLW+3nAsl6SUCq0aTkIpFQpcApx28thlC0prXQH8uSRSGvB5NZZE8lpKqdnACqC5UipbKXWz6UwuqhcwGutfuRuqfl1uOpSLqg8sU0ptwvoH4xKttYxQi5qIAX5XSm0EVgPfaK2/P92DXXbMXAghhHdz2SMoIYQQ3k0KSgghhEuSghJCCOGSpKCEEEK4JCkoIYQQLkkKSgghhNMopSKVUndW57FSUEIIIZwpEpCCEkII4XImAo2rbpJ/5UwPlBt1hRBCOE3VLgKLqrNvnRxBCSGEcElSUEIIIVySFJQQQghnKgTCqvNAKSghhBBOo7U+DCxXSm2RIQkhhBBuSY6ghBBCuCQpKCGEEC5JCkoIIYRLkoISQgjhkqSghBBCuCQpKCGEEC5JCkoIIYRL+n8fLeJm4nYdCwAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "y = v0 * t * sin(φ) - (1/2) * g * t**2 + 2\n",
    "y = y.subs(v0, 30)\n",
    "y = y.subs(φ, deg2rad(45))\n",
    "y = y.subs(g, 9.81)\n",
    "\n",
    "dy = diff(y, t)\n",
    "\n",
    "p1 = plot(y, xlim=(0,5), ylim=(0, 25), show=False)\n",
    "p2 = plot(dy, line_color=\"r\", show=False)\n",
    "p1.extend(p2)\n",
    "p1.show()"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Edit Metadata",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
