# Documentation

Python documentation: [https://docs.python.org/3/](https://docs.python.org/3/)

Numpy documentation: [https://numpy.org/doc/stable/reference/](https://numpy.org/doc/stable/reference/)

Matplotlib documentation: [https://matplotlib.org/contents.html](https://matplotlib.org/contents.html)

Scipy documentation: [https://docs.scipy.org/doc/scipy/reference/index.html](https://docs.scipy.org/doc/scipy/reference/index.html)

Pandas documentation: [https://pandas.pydata.org/docs/](https://pandas.pydata.org/docs/)

Seaborn documentation: [https://seaborn.pydata.org/api.html](https://seaborn.pydata.org/api.html)

Statsmodels documentation: [https://www.statsmodels.org/stable/api.html](https://www.statsmodels.org/stable/api.html)

Git documentation: [https://git-scm.com/docs](https://git-scm.com/docs)

PEP8: [https://www.python.org/dev/peps/pep-0008/](https://www.python.org/dev/peps/pep-0008/)

PEP8 summary: [https://realpython.com/python-pep8/](https://realpython.com/python-pep8/)
