# Quickref

For a fast overview of the Python language also take a look at [this](https://learnxinyminutes.com/docs/python/) website.
This page has a quick overview of common methods and operations, for the comprehensive guides always
check the documentation.

## Operators
Operators do different things in different contexts as Python supports operator overloading, 
for numbers they do:

| Operation | Syntax | Augmented |
|:----------|:-------|:----------|
| Addition         | a = a + b | a += b |
| Subtraction      | a = a - b | a -= b |
| True division    | a = a / b | a /= b |
| Integer division | a = a // b | a //= b |
| Multiplication   | a = a * b | a \*= b |
| Exponentiation   | a = a ** b | a \**= b |
| Modulo           | a = a % b | a %= b |
| Matrix multiplication | a = a @ b |  |

Example:
```python
a = 1 + 2  # 3
a += 5  # 8, inplace operator
a % 3  # 2, remainder division
```

## Comparisons
| Comparison | Python | MATLAB |
|:----------|:-------|:----------|
| Greater than | > | > |
| Smaller than | < | < |
| Greater than or equal | >= | >= |
| Smaller than or equal | <= | <= |
| Equal | == | == |
| Not equal | != | ~= |

Example:
```python
1 > 2  # False
5 <= 5  # True
```

## Falsy values
- Empty lists \[ \], tuples (), dicts {}, and sets set()
- Empty strings ""
- Empty ranges
- Zero
- None
- False

Example:
```python
bool([])  # False

my_dict = {}

if not my_dict:
    print("This dict is empty")
#This dict is empty
```

## Lists

Making a [list](https://docs.python.org/3/library/stdtypes.html#list):
```python
# can be constructed with
list1 = [1, 2, 3]  # using brackets
list2 = [x * 2 for x in list1]  # using a list comprehension
list3 = list((1, 2, 3))  # using the list constructor

```

Common methods and operators include:


| Method/Operator        | Result                                       |
|:-----------------------|:---------------------------------------------|
| .append(object)        | append object to the end of the list         |
| .extend(list2)         | extend list with another list                |
| .insert(index, object) | insert object _before_ index                 |
| .pop(index)            | return and remove item at index              |
| .reverse()             | reverses the list _inplace_                  |
| .sort()                | sorts the items of the list _inplace_        |
| .count(value)          | counts occurences of the value in the list   |
| .index(value)          | finds the first occurence of value           |
| .copy()                | returns a copy of the list                   |
| .clear()               | empties the list                             |
| list * n               | repeats the list n times                     |
| list1 + list2          | returns the concatenated lists               |
| object in list         | returns True if object is in the list        |
<!-- #endregion -->

## Dicts

Making a [dict](https://docs.python.org/3/library/stdtypes.html#typesmapping):
```python
# can be constructed with
dict1 = {"a": 1, "b": 2}
dict2 = {key: value for key, value in zip(key_iterable, value_iterable)} # using a dict comprehension
dict3 = dict([["a", 1], ["b", 2]])   # using the list constructor

```

Common methods and operators include:


| Method                 | Result                                                  |
|:-----------------------|:--------------------------------------------------------|
| .keys()                | gets all the keys                                       |
| .values()              | gets all the values                                     |
| .items()               | gets all key, value pairs                               |
| .get(key, default)     | rreturns the value for a key if present or the default  |
| .update(key, value)    | add or change a key value pair                          |
| key in dict            | returns True if the key is present                      |

The objects returned by dict.keys(), dict.values() and dict.items() are view objects. They provide a dynamic view on the 
dictionary’s entries, which means that when the dictionary changes, the view reflects these changes. Dictionaries can 
also be concatenated in the following way:

```python
# From Python 3.5 you can also use this unpacking option
{'a': 1, **{'b': 2}}  # => {'a': 1, 'b': 2}
{'a': 1, **{'a': 2}}  # => {'a': 2}
```

## Sets
A set object is an unordered collection of distinct hashable objects. Common uses include membership testing, 
removing duplicates from a sequence, and computing mathematical operations such as intersection, union, difference, 
and symmetric difference, for all operators and methods look [here](https://docs.python.org/3/library/stdtypes.html#set).

```python
set1 = {"Jack", " Sjoerd"}  # created with curly braces like a dict, but without key: value mapping
set2 = set(["Jack", "Sjoerd"])  # create with set constructor

```

| Method/operator        | Result                                                  |
|:-----------------------|:--------------------------------------------------------|
| set <= other           | Test whether every element in the set is in other.   |
| set >= other           | Test whether every element in other is in the set.   |
| set \| other           | Return a new set with elements from the set and all others.              |
| set & other            | Return a new set with elements common to the set and all others.         |
| set - other            | Return a new set with elements in the set that are not in the others.    |
| set ^ other            | Return a new set with elements in either the set or other but not both.  |
| .add(elem)             | Add element elem to the set.                            |
| .remove(elem)          | Remove element elem from the set.                       |
| .pop()                 | Remove and return an arbitrary element from the set.    |
| .clear()               | remove all elements from the set.                       |


## Strings

Python has very powerful [string](https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str) manipulation options. Strings are used for textual data and are denoted by single ``'string' or "string"``, double ``''string'' or ""string""``, or triple ``'''string''' or """string"""`` quotes. Triple quoted string can span multiple lines. The option to choose between ``' and "`` is especially nice when working with textual data (e.g. print("This won't break"), print('This'll break')). We could probably spend an entire lecture on handling strings. Luckily you already have some experience from DataCamp. First there are the string operators:
- the + operator: concatenates strings
- the * operator: repeats strings n times
- the in operator: checks if string is present in string

Then there are the built-in functions that work with strings:
- chr(): converts an integer to a character
- ord(): converts a character to an integer
- len(): returns the length
- str(): tries to coerce object to a string, to do the opposite use float() or int() on a string

Then there are the [methods](https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str) for strings, methods are functions that belong to an object:
- str.capitalize():
- str.casefold():
- str.center():
- str.cound():
- ....
- str.zfill():

Formatting:
```python
n = 20
m = 25
print('The product of %d and %d is %d' % (n, m, n * m))  # oldest
print('The product of {0} and {1} is {2}'.format(n, m, n * m))  # also old, but still useful
print(f'The product of {n} and {m} is {n * m}')  # fresh
# The product of 20 and 25 is 500
```


## For-loops

```python
# General form
for <var> in <iterable>:
    <statement(s)>

# Example
my_list = ["spam", "eggs", "bacon"]
for item in my_list:
    print(item)
# spam
# eggs
# bacon    

# Example with else
my_list = ["spam", "eggs"]
for item in my_list:
    print(item)
else:  # only triggers when iterable is exhausted, nobody uses this
    print("Done.")
# spam
# eggs
# Done.
```

## Control flow

```Python
# Let's just make a variable
some_var = 3.14

# This prints "some_var is smaller than 10"
if some_var > 10:
    print("some_var is totally bigger than 10.")
elif some_var < 10:    # This elif clause is optional.
    print("some_var is smaller than 10.")
else:                  # This is optional too.
    print("some_var is indeed 10.")
```


## Functions

```python
# General form
def function_name(input_par1, input_par2=default_value, ...):
    """Docstring"""
    # function block
    return output1, output2, ...


# Example
def square(x):
    """Squares an integer or float."""
    return x * x
```

```{note}
By convention function names -just like variable names- are lowercase with an underscore to separate words.
```

## Glossary
Knowing the right terms can help you look up stuff on the internet much quicker {cite}`howtothink`!

```{glossary}

[boolean](https://docs.python.org/3/library/stdtypes.html)
    Boolean data type is a data type that has one of two possible values, True or False

[comment](https://en.wikipedia.org/wiki/Comment_(computer_programming))
    Information in a program that is meant for other programmers (or anyone reading the source code) and has no effect on the execution of the program (comments in Python start with #).

[float](https://en.wikipedia.org/wiki/Floating-point_arithmetic)
    A Python data type which stores floating-point numbers. When printed in the standard format, they look like decimal numbers.

[function](https://en.wikipedia.org/wiki/Subroutine)
    A function is a named sequence of statements that belong together. Their primary purpose is to help us organize programs into chunks that match how we think about the problem.

[integer](https://en.wikipedia.org/wiki/Integer)
    A whole number.

[Integrated Development Environment](https://en.wikipedia.org/wiki/Integrated_development_environment)
    A computer program that helps you write code (such as Spyder). 

[method](https://stackoverflow.com/questions/3786881/what-is-a-method-in-python)
    A function which is attached to an object and can be accessed with the dot operator.

[string](https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str)
    A Python data type that holds a string of characters.

[type casting](https://en.wikipedia.org/wiki/Type_conversion)
    Converting one data type to another, for example a string to a float by using `float()`.

```