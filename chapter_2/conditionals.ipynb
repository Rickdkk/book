{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Conditionals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Please grab your BMI script from the latest assignment. We'll expand it to tell us whether the BMI is too low, normal, or too high. To do so, we will need to add some 'logic' to the script. It will have to recognize out input and print a statement based on the BMI calculation. \n",
    "\n",
    "Consider the following flowchart of the script:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{figure} ../images/flowchart_BMI.png\n",
    "---\n",
    "name: flowchart_BMI\n",
    "height: 450px\n",
    "---\n",
    "Flowchart of the BMI script with conditionals\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Rectangles with rounded corners are used for input/output, normal rectangles for processing, and diamonds for blocks with alternating paths. In this chapter, we are mainly interested in those blocks."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Booleans"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the flowchart we used Yes and No to denote whether a condition was met. Python uses the words `True` and `False`. These are also called booleans. In Python, a {term}`boolean` is a special kind of integer, True is equal to 1 and False is 0. We can use these booleans to control the flow of our program. In other words, we can use them to go into the desired path on the flowchart. We already saw some booleans in the previous chapter (e.g.):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\"aBCd\".islower()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Comparisons"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are more operations that result in booleans. If we want to compare values we have to use a comparison operator. These are very similar to the ones used in math:\n",
    "\n",
    "| Comparison | Python |\n",
    "|:----------|:-------|\n",
    "| Greater than | > |\n",
    "| Smaller than | < |\n",
    "| Greater than or equal | >= |\n",
    "| Smaller than or equal | <= |\n",
    "| Equal | == |\n",
    "| Not equal | != |\n",
    "\n",
    "For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "1 < 10"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "1 > 10"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also chain them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "1 < 2 < 10"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{admonition} Assignment\n",
    "What do the following statements return?\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {
    "tags": [
     "hide-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "10 <= 10"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {
    "tags": [
     "hide-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "9 <= 10 < 11"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {
    "tags": [
     "hide-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "10 != 10"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Boolean operators"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes chaining is not an option or not the most readable option. Therefore, Python also has three boolean operators:\n",
    "\n",
    "- `and`\n",
    "- `or`\n",
    "- `not`\n",
    "\n",
    "They are plain English words and they do what you would expect based on their English counterparts. The `and` operator returns `True` only if both expressions were `True`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "True and True"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "True and False"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "1 < 10 and 5 < 10"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `or` operator returns `True` if at least one of the expressions evaluates to `True`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "True or True"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "True or False"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "False or False"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `not` operator negates the next boolean, turning `True` into `False` and `False` into `True`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "not True"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "not False"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "True and not False"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Membership\n",
    "\n",
    "There is actually one last operator and it is slightly different than the others. The `in` operator checks whether an object is part of another object. This works on many objects but it is best demonstrated with a string:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\"Hello\" in \"Hello world\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\"Hllo\" in \"Hello world\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, the `in` operator looks for an exact match in the sequence. It does not take a lot of imagination to see that this is incredibly useful. We will learn about different collections than strings later and we will see that this operator also works for those!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conditional evaluation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now for the truly juicy part: conditional evaluation. At the very top I mentioned that we wanted to print either \"underweight\", \"normal weight\", or \"overweight\" based on the BMI calculated in our script. This means that we have to compare the calculated BMI with a set of cutoff values and then only execute a part of our script based on the result. Here is the anatomy of the `if`-`elif`-`else` syntax:\n",
    "\n",
    "\n",
    "```python\n",
    "if some_condition:\n",
    "    print(\"something\")\n",
    "elif some_other_condition:\n",
    "    print(\"something else\")\n",
    "else:\n",
    "    print(\"none of the above\")\n",
    "```\n",
    "\n",
    "If one expression is `True` then the corresponding block is evaluated. Otherwise, the next expression is checked. If neither are `True`, the `else` block is evaluated. You can use as many `elif` blocks as you need, but there is only one `else` block. You can also opt to only include the `if` block without the others. Here it is in action with a simple example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x is equal to 10\n"
     ]
    }
   ],
   "source": [
    "x = 10\n",
    "\n",
    "if x == 10:\n",
    "    print(\"x is equal to 10\")\n",
    "elif x < 10:\n",
    "    print(\"x is smaller than 10\")\n",
    "else:\n",
    "    print(\"x is bigger than 10\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "``````{margin}\n",
    "```{note}\n",
    "Python uses whitespace to indicate which code is part of the if-statement, this means you Have to put in the tab (4 spaces) as shown above. Using a tab in Spyder will automatically put in four spaces for you.\n",
    "```\n",
    "``````\n",
    "\n",
    "In this case, the first expression is `True`. This means the first block is executed and all others are ignored.\n",
    "\n",
    "\n",
    "Now consider the next example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x is bigger than 10\n"
     ]
    }
   ],
   "source": [
    "x = 11\n",
    "\n",
    "if x == 10:\n",
    "    print(\"x is equal to 10\")\n",
    "elif x < 10:\n",
    "    print(\"x is smaller than 10\")\n",
    "else:\n",
    "    print(\"x is bigger than 10\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `if` and `elif` statement both evaluate to `False`, thus the code in the `else` block is executed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{admonition} Assignment\n",
    "You want to add a print statement to the BMI-calculator from earlier. People with BMI values lower than 18.5 are underweight, between 18.5 and 25 are normal weight, and equal to or higher than 25 are overweight {cite}`worldglobal`. Take a look at the flowchart and implement the control flow. You should get something like this:\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {
    "tags": [
     "hide-input"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Please input a height and weight to determine your BMI!\n",
      "\n",
      "What is the weight (kg)? 90\n",
      "What is the height (m)? 1.85\n",
      "\n",
      "The BMI for the specified weight and height is: 26.3\n",
      "You are overweight!\n"
     ]
    }
   ],
   "source": [
    "print(\"Please input a height and weight to determine your BMI!\\n\")\n",
    "\n",
    "weight = float(input(\"What is the weight (kg)? \" ))\n",
    "height = float(input(\"What is the height (m)? \"))\n",
    "\n",
    "bmi = weight / height**2\n",
    "\n",
    "print(f\"\\nThe BMI for the specified weight and height is: {bmi:.1f}\")\n",
    "\n",
    "if bmi < 18.5:\n",
    "    print(\"You are underweight!\")\n",
    "elif bmi < 25:\n",
    "    print(\"You are normal (healthy) weight.\")\n",
    "else:\n",
    "    print(\"You are overweight!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Truthy/falsy\n",
    "\n",
    "One final concept to consider is that of 'truthy' and 'falsy' values. A very useful feature is the inherent 'falseness' of zero and empty data structures. To observe this effect we have to write an `if` statement, or we can coerce the object to a boolean by type casting it. For example, for numbers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "bool(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Empty strings:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "bool(\"\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Empty lists:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 32,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "bool([])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This feature can come in very handy at times. For example, if we use `input` but the response is an empty string we can print a reply without having to compare it to an empty string. The code will read much like English:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Please enter your name: \n",
      "No input received!\n"
     ]
    }
   ],
   "source": [
    "reply = input(\"Please enter your name: \")\n",
    "\n",
    "if not reply:\n",
    "    print(\"No input received!\")"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Edit Metadata",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
