# 2. Basic Python

In this chapter we'll introduce you to some of the basic concepts of the Python language. It's quite an extensive list 
but don't be scared because we'll take it step by step and for every topic there will be an assignment to consolidate
your knowledge, and more importantly, where you can see that going up to that point was already worth it because we can 
apply our knowledge to real-world problems!

- Doing calculations: basic types and variables
- Manipulating text: strings
- Storing multiple values: lists/tuples
- Mapping values: dicts
- Repeating commands: loops
- Build in some intelligence: conditionals
- Write less code: functions
- Project ideas and leetcode/CodeWars!

You are advised to make a new script (`ctrl+N`) for (at least) each section and to split up your code in sensible code 
blocks. It is a good idea to dedicate a directory on your computer for your Python code. You are responsible for 
keeping everything tidy which is a heavy burden to carry. I still make a mess out of every project, but I trust you to
be better! Also make sure you put comments (`#`) in your code so you know what you did. This way you can use your own 
files for later reference!

While it is more common to use docstring in modules and functions (we will cover this later). It would be a good idea at 
this point to include a header in each of your scripts:

```python
"""Title of the script

Description: A short description of the script
Author: Your Name
Created: year/month/day
License: MIT
"""
```

Enough talking, I promised you coding so let's jump into the next section!
