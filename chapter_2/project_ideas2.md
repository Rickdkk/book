# Project ideas CH2

The best way to learn a language is to speak the language. You have to code in order to retain and refine your Python skills.
There is no point in only doing coding assignments. This will become boring pretty quickly and you will lose interest. 

Instead,
it is best to find a coding project that means something to you. I cannot guess what you are interested in, maybe you like
stories and want to make a choose your own adventure script, maybe you want to write a script that cleans up your computer,
maybe you want to automate your e-mails, or maybe you just want to do data analysis (in that case just move on). I can, however,
give some suggestions for project ideas. 

So each chapter I will try to give you some ideas that should work for the level of knowledge that you are at at that 
point in time. 

## Project 1
Random number game

## Project 2
File sorter

## Project 3
PDF splitter

## Project 4
Prime generator

## Project 5
Better converter

## Challenges
There are a number of platforms on the internet that you can use to sharpen your skills. These are fun, challenging, and 
they add a form of gamification to the process. To name a few:

- [codewars](https://www.codewars.com/): Challenge yourself on kata, created by the community to strengthen different s
kills. Master your current language of choice, or expand your understanding of a new one.
- [HackerRank](https://www.hackerrank.com/domains/python): Join over 11 million developers, practice coding skills, prepare
for interviews, and get hired.
- [LeetCode](https://leetcode.com/): LeetCode is the best platform to help you enhance your skills, expand your knowledge
and prepare for technical interviews.