# 1. Getting started

Chapter 1 is really short. First we need to make sure you have a working Python installation on your computer. If you 
use a Mac or Linux operating system you will likely already have Python installed. It probably won't have a lot of 
packages installed by default though, especially not data-analysis packages. Seeing as we're going to use Python for 
data analysis it would be nice to have everything we need in one place so you still might want to follow the instructions 
on the next page.

- Step one: Install Python
- Step two: Get to know Spyder
- Step three: Start programming!

## About Python
```{margin} Library
A library is a collection of packages that add extra functionalities.
``` 
[Python]( https://www.python.org/about/) is a general-purpose programming language that can be used for many different 
tasks. It is a high-level language: in general these are easier to read and write for users at the cost of being slower
(don't worry about this though). Other high-level languages you might have heard of are C#, Java, or R. Python was 
developed by Guido van Rossum at the Centrum Wiskunde & Informatica in The Netherlands about 30 years ago. It is now 
managed by the [Python Software Foundation]( https://www.python.org/psf/) and is often described as a "batteries 
included" language due to its comprehensive standard *library*. The Python documentation is hosted 
[here]( https://docs.python.org/3/) and it includes tutorials specific to the Python language and built-in libraries. 

A final note,  when you have to look up something on the internet, which is 85% of programming, you may encounter Python 2 
and 3 in the wild.. Python 2 is now deprecated so you are highly advised to just use the latest version. Just know it 
exists and that its syntax is slightly different than the version we will use (most notably the range and print function). 

## Why Python

Python is a powerful programming language. Due to its clear, easy-to-read, and consistent syntax it is easy to learn. 
There is a lot of emphasis on code readability, users are forced to use indentation, there are no brackets, and there 
are words where other languages would use symbols: 

```python
# Python implementation of "Hello world!"
print("Hello world")
```

```java
// "Hello world" in Java
public static void main(String[] args) {
    System.out.println("Hello world!");
}
```

It allows you to run your code on a Windows, Mac, or Linux machine without having to pay for expensive software licenses. 
Python plays really well with other programming languages so it can be mixed with C or Fortran for speed (like in the packages
`Numpy` and `Scipy`). It is easy to extend the functionality of Python and download (or publish) additional packages. 
The [Python Package Index](https://pypi.org/) currently contains over 250,000 packages. As a result, using these packages,
it can do pretty much anything and is widely used for:

- Web programming (e.g. [Django](https://www.djangoproject.com/), [Flask](https://flask.palletsprojects.com/en/1.1.x/))
- Making graphical user interfaces (e.g. [Pyside2](https://wiki.qt.io/Qt_for_Python), [Tkinter](https://docs.python.org/3/library/tk.html), [PySimpleGUI](https://pysimplegui.readthedocs.io/en/latest/))
- Game development (e.g. [pygame](https://www.pygame.org/news))
- Automation and rapid application development (e.g. [pyqtgraph](http://www.pyqtgraph.org/))
- Web scraping (e.g. [Scrapy](https://scrapy.org/), [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/), [Selenium](https://selenium-python.readthedocs.io/))
- Machine learning and neural networks (e.g. [Scikit-learn](https://scikit-learn.org/stable/), [Keras](https://keras.io/))
- Scientific programming (see below)

## About scientific Python
```{margin} Distribution
A distribution is a collection of software. In this specific case, it is Python + all scientific programming packages.
```

The [Scipy]( https://www.scipy.org/) stack \[{numref}`scipy_stack`\] contains the core scientific programming packages that f
orm the basis of pretty much all scientific computing in Python. These packages use C and Fortran (low-level languages) 
under the hood and are therefore much faster, but you get to use them through Python which is way more ergonomic. When you 
finished this book you will be familiar with most of these packages! They do not come with Python by default, but we 
will use a *distribution* that does include these packages. Go to the next page and get ready to install Anaconda.

```{figure} ../images/scipy_stack.png
---
name: scipy_stack
align: center
---
The scientific Python stack.
```