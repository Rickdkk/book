# Anaconda

More snakes... There are many ways to use Python and you will be using [Anaconda](https://www.anaconda.com/products/individual#download-section). 
It is a free and open-source distribution of Python specifically targeted at scientific computing. It includes most of 
the popular data science packages and makes it very easy to install others if necessary. It simplifies package management 
and deployment and is widely used by researchers. It also provides you with a number of Integrated 
Development Environments (IDEs) like [Spyder](https://www.spyder-ide.org/) and [Jupyter](https://jupyter.org/) that are 
more powerful than Python's built-in one ([IDLE](https://docs.python.org/3/library/idle.html)). In short, it just works.
You  should get the latest Python version (3.6 or higher).

```{figure} ../images/Anaconda_Logo.png
---
height: 75px
name: anaconda
align: center
---
Anaconda logo
```
```{admonition} Assignment
Grab the latest version of Anaconda from their [website](https://www.anaconda.com/products/individual#download-section), 
follow the directions and install it on your system.
```

## Testing
Let's see if you installed Python correctly. In typical programmer tradition we will start with trying to get Python to 
return "Hello world". To do so, we need to run Python and use the `print` function, which *prints* something to your 
terminal.

```{admonition} Assignment
Launch the terminal (on Windows use the Anaconda Prompt program instead). Now check your Python installation by typing
`python --version`. If this replies with your Python version it means that you have correctly installed Python.

Now it's time to launch Python. You can launch python by typing `python` or `ipython`.  You can now input Python commands into your terminal. Lets start with the
following command: `print("Hello world")`. You can exit Python by typing `quit()`
```

```{figure} ../images/python_prompt.png
---
name: python terminal
align: center
---
Python in the terminal
```

By typing python you have launched what is called a read-evaluate-print session. What happened here is that we launched 
the Python session and it waits for us for input. We then asked the interpreter to translate our Python command to machine 
code. That code is then executed by your computer and the result is returned to you. The interpreter only speaks Python 
so if you enter an unknown command it will give you an error. 

## Python as a calculator
I sure hope you didn't hit an error there, if not, we can use Python to do some math! We will get into the details of the
syntax later but for now assume you can use your normal mathematical operators. Python stores the result of your last
operation in the underscore character `_`.

```{admonition} Assignment
- Launch Python in your terminal
- Calculate the outcome of 2 + 2
- Calculate the outcome of 5 * 10
- Divide 10 by 3
- Use your last answer and add 2
- Exit python by typing `quit()` or by closing the terminal.
```

This should be fairly straightforward. Using Python as a calculator is really useful and much more powerful than the one
included with your operating system. 


## Up next: Development Environment

While programming in the terminal is nice, there are more efficient ways. Luckily there is software that helps you write
software, they are called IDEs (Integrated Development Environments). Your IDE for this course will be 
[Spyder](https://www.spyder-ide.org/) as it is pretty beginner friendly and it shares a lot of similarities with other 
popular scientific IDEs such as R-studio and the MATLAB IDE. Spyder comes with Anaconda, so it should already be 
available to you. Other popular IDEs for Python include, but are not limited to:

- [Pycharm](https://www.jetbrains.com/pycharm/) - most complete IDE, made by JetBrains, students can get the professional version for free
- [Jupyter](https://jupyter.org/) - interactive data science environment, can be used to present data or code (e.g. this book)
- [VS Code](https://code.visualstudio.com/) - expandable IDE compatible with most programming languages by Microsoft
- [Thonny](https://thonny.org/) - simple IDE targetted at learning programming

You can also pick one you like, but if you have issues with your IDE you will have to figure them out yourself.
