# Spyder

Spyder is an *{term}`Integrated Development Environment`* (IDE). In short, it is a program that helps you write code by giving you 
suggestions, highlighting your syntax, warning you, and providing documentation when needed {cite}`fangohr2018spyder`. 
Spyder is specifically made for scientific computing in Python and it's also written in Python! If you want more information 
after working through this subchapter: it has a built-in tutorial that can be accessed through `Help > Spyder tutorial` 
and the documentation is hosted [here](https://docs.spyder-ide.org/).

```{admonition} Assignment
Open up Spyder on your computer. Go to `Help > Interactive tours > Introduction tour` for an introduction to Spyder.
```

## Windows

If you did the tour you should have a good feel about the different windows in Spyder. In short:

- **The Script Editor:** allows you to write code in scripts so you can reuse the code
- **The Console:** executes Python code directly or from your script, also displays the result
- **The Variable Explorer:** allows you to examine the variables defined
- **The Help:** shows you the documentation of the packages you are using

```{figure} ../images/spyder_default_layout_annot.png
---
name: Spyder_layout
align: center
---
Default Spyder layout
```

```{margin} Enlarge figure
Open the figure in a new tab if it is too small! If your layout looks different go to `View > Window layouts` and set it 
to default.
```

## Using the console

The console works the same as the one you have been using in the terminal. You can input a Python command and it will 
respond to you with the answer. Some useful shortcuts are:

- `ctrl+L`: clears your console (note that this does not undo your commands!)
- `ctrl+C`: interrupts Python, can be useful when you accidentally start a very long command or have an infinite loop
- `ctrl+alt+R`: clear all variables
- `ctrl+.`: totally restarts the Python interpreter

```{admonition} Assignment
Type `tau = 2 * 3.14` in the IPython console to make a variable named "tau". Now print tau by typing its name or by 
inserting it in the `print` function. Now clear the console and print tau again, note how tau is still in memory. Now 
clear all variables and try again, what happened?
```

## Using a script
```{margin}
```{Tip}
You can check out all keyboard shortcuts in `Help > Shortcuts Summary` and navigate like a pro.
```

Typing in the console is nice and all, but it is not very reproducible. Python can also run commands from text files 
(.py files) and we can use those files to run the same commands again. We can make and edit those files in the script 
editor. If you run code from your script, it gets sent to the IPython console we used before, so we can mix putting the 
essentials in the script and playing around in the console if we see fit.

- `ctrl+N`: make a new script
- `ctrl+S`: save the script
- `F5`: run all contents of the script
- `F9`: run selected contents

```{admonition} Assignment
Make a new file called "hello_world.py". Add a print statement for "Hello world" and another print statement for 
"Goodbye world". Run the entire file with `F5` or by pressing the 'play' button. Now run each command separately with 
`F9` or the button in the top bar. 
```

## Using code-blocks

Sometimes, especially when working with big datasets, it is a good idea to not rerun the entire script (`F5`) every time you 
change something. In Spyder you can use code cells to break up your code in sections. Code cells are denoted with 
`#%%`, e.g.:

```python
#%%
print("The first code block")

#%%
print("The second code block")
```

You can use ``ctrl+enter`` to run a code cell or ``shift+enter`` to run a code cell and move to the next one. 
Alternatively, you can press the buttons in the top bar like a savage.

```{admonition} Assignment
Add your two print statements to separate code blocks and run them separately.
```

## Variable explorer

The Variable Explorer shows the namespace contents (all global object references, such as variables, functions, modules, 
etc.) of the currently selected IPython Console session, and allows you to interact with them through a variety of 
GUI-based (graphical) editors. For example, running the following code will give you:

```python
my_integer = 1
my_float = 1.5
my_string = "Hello world"
my_list = [my_integer, my_float, my_string]
```

```{figure} ../images/spyder_variable_explorer_crop.png
---
name: Spyder
align: center
---
The variable explorer
```

```{caution} 
You can edit variables in and plot from the variable explorer. However, avoid doing this because doing so will make your 
scripts non-reproducible!
```

If you play around a lot your environment can get pretty cluttered. You can remove all variables with the button on the 
top right side of your console or on top of the variable explorer. Alternatively, you can press `ctrl+alt+R` or type 
`%clear` in the console, which do the same thing. If you really mess up, you need to restart the kernel, there is a 
button in the burger menu on the Console, or use the shortcut `ctrl+.`.

## Help

Now I can hear you thinking, this is getting quite a lot: help...! Spyder has got your back. You can trigger help by 
manually entering the object’s name into the Object box in the help menu, pressing the configurable help shortcut 
(`ctrl+I`), or after typing a left parenthesis (`(`) after a function or method name (when enabled). Automatic help can 
be individually enabled for both the Editor and the Console under `Preferences > Help > Automatic Connections`, and 
turned on and off dynamically via the lock icon in the top right corner of the Help pane. For example, if we want to 
know what the `print` function does:

```{figure} ../images/help_print.png
---
name: print help
align: center
---
Help for the built-in print function
```

```{admonition} Assignment
Try this for yourself, look up the function `len()`, what does it do? Now use the function on your variable `my_string`, 
did it return what you expected based on the documentation?
```

## Writing beautiful code

We all want to write beautiful code and no matter how hard we try there will always be people that hate our code as a 
lot of it comes down to taste. It is, however, good practice to abide by the basic principles set by the
[Style Guide for Python source code](https://www.python.org/dev/peps/pep-0008/) (also referred to as PEP-8; for a quick
overview click [here](https://realpython.com/python-pep8/)). Almost all programmers and 
packages use this style which, for example, it makes it easy to recognize the name of a function (snake_case) from a class 
(PascalCase). Because there are quite a few rules in PEP-8 and because writing beautiful code is not easy, Spyder can 
aid you here:

**For Spyder 3:**

`Tools > Preferences > Editor > Code Introspection/Analysis`

and activating the option called `Real-time code style analysis`.


**For Spyder 4:**

`Tools > Preferences > Completion and linting > Code style`

and activating the option called `Enable code style linting`.


```{admonition} Assignment
For your convenience and for my sanity, enable the automatic style checking in Spyder! Change the maximum line length to
121. Even if you don't want to write beautiful code: it also saves you from making common errors, so there is really no 
excuse on not enabling this feature!
```

```{figure} ../images/spyder_code_style.png
---
name: code_style
align: center
---
Code style menu
```

```{margin} Line length
The PEP-8 rule for line length can be mostly ignored (up to 121 is fine-ish), it (probably) stems from the time monitors 
were small and terminals were 81 characters wide, practicality does beat purity sometimes. 
```

## Are we there yet?

There are no additional exercises for this chapter. Now that we have you all set up we can finally start coding! 
Next chapter: an introduction to Python.
