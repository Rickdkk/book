# Overview

The book for Python for Human Movement Sciences. It was built using jupyterbook version 0.7.0b.

```shell script
rm -rf ./_build
jupyter-book build .
python gitlab.py
```

Note that this currently only works on Linux. The Gitlab.py script is used to replace references to Github.