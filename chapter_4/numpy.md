(chapter4)=
# 4. Arrays

- Arrays
- Plotting basics
- Basic functions and broadcasting
- Indexing and slicing
- Generating arrays
- Multidimensional arrays
- Views and copies

mention printoptions
plot sine and cosine
generate signals
calibrate a device (goniometer/loadcell)
apply rotations
