# 7. Dataframes

- Tabular data
- Indexing
- Aggregating data
- Manipulating strings
- Reading and writing data
- Slicing, grouping, and sorting
- Plotting